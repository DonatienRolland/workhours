import React, { Component } from 'react'
import './date_picker.css';
import DatePicker from "react-datepicker";
import { registerLocale } from  "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import fr from "date-fns/locale/fr";
registerLocale("fr", fr);

class DatePickerComponent extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }


  handleChange(date_value){
    const today = new Date()
    let type = "date"
    if (daySelected(date_value) === daySelected(today) ) {
      type = "Aujourd'hui"
    }
    this.props.onDateChange({date: date_value, type: type});
  };


  render(){
    const pickededDate = this.props.date
    const typeDate = this.props.type
    let selectedDate = <p className="date-slected">Le {daySelected(pickededDate)}</p>
    if (this.props.type !== "date"  ) {
      selectedDate = <p className="date-slected">{typeDate}</p>
    }
      return(
        <label>
          {selectedDate}
          <DatePicker
              selected={pickededDate}
              className="react-datepicker-input"
              locale="fr"
              dateFormat="P"
              onChange={this.handleChange}
            />
        </label>
      )
  }
}

function daySelected(value){
  let month = value.getUTCMonth() + 1;
  let day = value.getUTCDate();
  let year = value.getUTCFullYear();
  let selected = day + "/" + month + "/" + year
  return selected
}




export default DatePickerComponent
