import React, { Component } from 'react'
import './form.css';
import DatePickerComponent from '../datepicker/date_picker'
import PeriodeHours from '../periodeHours/periode_hours'
import ConclusionOfWork from './conclusion'

class EasyForm extends Component {
  constructor(props) {
    super(props);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleHoursChange = this.handleHoursChange.bind(this);
    this.state = {
        date: new Date(),
        format: ":",
        type: "Aujourd'hui",
        periodes: [{
            id: 2201,
            start: "00:00",
            end: "00:00"
          },{
            id: 2221,
            start: "00:00",
            end: "00:00"
          }],
    };
  }


  handleDateChange(value){
    this.setState(value)
  }
  handleHoursChange(value){
    const periodeIds = this.state.periodes.map(periode => periode.id)
    let result
    if (periodeIds.includes(value.id)) {
      result = this.state.periodes.map(periode => periode.id === value.id ? value : periode)
    } else {
      value.id = Math.floor(Math.random() * 100) + 1
      result = this.state.periodes.concat(value)
    }
    this.setState({periodes:result})
  }

  render(){
    let selectedDailyworks = this.state
    const lastPeriode = selectedDailyworks.periodes[selectedDailyworks.periodes.length - 1]
    let addperiode
    if (lastPeriode.end !== "00:00") {
      addperiode = <PeriodeHours
        periode={{id:3221, start:"00:00", end:"00:00"}}
        key={232}
        index={3}
        format={this.props.format}
        onTimeChange={this.handleHoursChange}
        />
    }

    const listHours = selectedDailyworks.periodes.map((periode, index) =>
       <PeriodeHours
          periode={periode}
          key={periode.id}
          index={index}
          format={this.props.format}
          onTimeChange={this.handleHoursChange}
          />
      );
    return(
      <div className="daily-form">
        <DatePickerComponent
          date={this.state.date}
          type={this.state.type}
          value={this.state.date}
          onDateChange={this.handleDateChange}
          />
        <p>j'ai </p><p> travaillé </p>
        {listHours}
        {addperiode}
        <ConclusionOfWork
          time={this.state.periodes}
          format={this.state.format}
        />
      </div>
      )
  }
}



export default EasyForm
