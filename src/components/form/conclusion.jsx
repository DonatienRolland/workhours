import React, { Component } from 'react'
import './form.css';


class ConclusionOfWork extends Component {

  render(){
    const time = convertMinToTime(this.props.time, this.props.format)
    let conclusion
      if ( time ) {
        conclusion = <p className="conclusion">J'ai donc travaillé <strong>{time}</strong> et il me reste TODO cette semaine  à réaliser.</p>
      } else {
        conclusion = <p> Alors, en vacances ? </p>
      }
    return (
      <div> {conclusion} </div>
      )
  }
}

function convertMinToTime(value, format){
  if (value === undefined ) { return false }
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  let totalMin
  if ( value.length > 0) {
    const array = value.map((periode, index) =>
      periode.end !== "00:00" ? convertStringToMin(periode.end, format) - convertStringToMin(periode.start, format) : ""

    )
    totalMin = array.reduce(reducer)
  } else { totalMin = convertStringToMin(value.end, format) - convertStringToMin(value.start, format) }
  if (totalMin > 0) {
    return Math.floor(totalMin /60).toString() + "h" + (totalMin % 60).toString()
  } else {
    return false
  }
}

function convertStringToMin(string, format){
  const h = parseInt(string.split(format)[0], 10)
  const min = parseInt(string.split(format)[1], 10)
  const total = min + h*60
  return total
}


export default ConclusionOfWork
