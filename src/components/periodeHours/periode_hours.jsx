import React, { Component } from 'react'
import TimeField from 'react-simple-timefield';
import './hours.css';


class PeriodeHours extends Component {
  constructor() {
    super()
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const name = event.target.name
    const value = event.target.value
    if (name === "startDate") {
      this.props.onTimeChange({
        id: this.props.periode.id,
        start: value,
        end: this.props.periode.end
      });
    } else {
      this.props.onTimeChange({
        id: this.props.periode.id,
        start: this.props.periode.start,
        end: value
      });
    }
  }


  render(){
    const style = {
      width: "110px"
    }
    let puis
    if (this.props.index !== 0) { puis = <p> puis </p>; }
    return(
      <div className="periode-form">
        {puis}
        <p> de</p>
        <TimeField
            className="hours-input"
            name={"startDate"}
            type="text"
            pattern="\d*"
            colon={this.props.format}
            onBlur={this.handleChange}
            style={style}
            value={this.props.periode.start}
            key={this.props.periode.id}
            id={this.props.periode.id}
        />
        <p>à</p>
        <TimeField
            className="hours-input"
            name={"endDate"}
            type="text"
            pattern="\d*"
            colon={this.props.format}
            onBlur={this.handleChange}
            style={style}
            value={this.props.periode.end}
            key={this.props.periode.end}
        />
      </div>
      )
  }
}



export default PeriodeHours
