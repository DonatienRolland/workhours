import React from 'react';
import logo from './logo.svg';
import './App.css';
import EasyForm from './components/form/easy_form'



function App() {

  return (
    <div className="App">
          <div className="login"><strong>Hello Donatien,</strong></div>
      <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
            <EasyForm/>
      </header>
    </div>
  );
}

export default App;
